+++
title = "Cara-oke"
description = "Karaoke avond"
date = "2019-09-20T18:00:00Z"
fburl = "https://www.facebook.com/events/2147101602081985/"
location = "JK Zomaar"
coordinates = "50.83637,4.24869"
price = "Gratis"
+++
![banner](/img/20190920-caraoke.jpg)

🎤 Kom liedjes zingen naar jaarlijkse traditie tot een kot in de nacht!

🎼 Eender welk liedje, wij hebben het!

🍻 Elke zanger(es) krijgt een frisse cara van het huis!*

*(Of iets straffer)
