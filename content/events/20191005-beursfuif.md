+++
title = "Beursfuif: Sport Edition"
description = "Thema-avond"
date = "2019-10-05T20:00:00Z"
fburl = "https://www.facebook.com/events/531177654118913/"
location = "JK Zomaar"
coordinates = "50.83637,4.24869"
price = "Gratis"
+++
![banner](/img/20191005-beursfuifsport.png)

Beursfuif @ JK Zomaar!
Kom verkleed als sporter en krijg iets gratis!
Feesten tot je net zo vaag bent als het thema!
