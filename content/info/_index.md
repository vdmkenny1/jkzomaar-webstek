+++
title = "Info"
date = "2019-09-03"
+++

## Adres
{{< partial "info/map" >}}
<br>Keperenbergstraat 37<br>1701 Itterbeek

## Openingsuren

Elke vrijdag vanaf 20u.

## Betalingsmogelijkheden

- cash
- bankkaart/kredietkaart

## Dranklijst

| Alcoholische Dranken | Prijs(€) |
| :------------------- | -------: |
| Stella               |     1.50 |
| Timmermans Kriek     |     1.50 |
| Cava                 |     2.00 |
| Geuze Boon           |     2.00 |
| Vedett               |     2.00 |
| Wijn (rood/rosé/wit) |     2.00 |
| Chimay Blauw         |     2.50 |
| Duvel                |     2.50 |
| Duvel Tripel Hop     |     2.50 |
| La Chouffe           |     2.50 |
| Tripel Karmeliet     |     2.50 |


| Alcoholvrije Dranken        | Prijs(€) |
| :-------------------------- | -------: |
| Kraantjeswater              |   Gratis |
| Water uit fles (Plat/Bruis) |     1.00 |
| Cola (Normaal/Zero)         |     1.50 |
| Ice Tea (Normaal/Green)     |     1.50 |
| Jupiler 0.0                 |     1.50 |
| Koffie                      |     1.50 |
| Orangina                    |     1.50 |
| Royco Soep                  |     1.50 |


| Eten         | Prijs(€) |
| :----------- | -------: |
| Chips        |     1.00 |
| Aïki Noodles |     2.50 |
