+++
title = "De Grote Heropening"
date = "2017-02-04"
+++
In 2016 ging het niet zo goed met de Zomaar. Het draaide op een laag pitje, en er was sprake van de keet te sluiten. Er was namelijk geen zicht op eventuele opvolging van het huidig bestuur, die er allemaal mee wouden ophouden.
Gelukkig daagde een nieuw, enthousiast bestuur op het nippertje op, om met een likje verf en de nodige [mediageile acties](https://www.nieuwsblad.be/cnt/dmf20170221_02742871) nieuw leven te blazen in de klup!

JK Zomaar was wederom.

{{< album 20170204-heropening  >}}
