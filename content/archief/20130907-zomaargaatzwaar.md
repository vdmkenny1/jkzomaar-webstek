+++
title = "Zomaar Gaat Zwaar 2013"
date = "2013-09-07"
+++
![banner](/img/20130907-zomaargaatzwaar-banner.jpg)

>JK Zomaar presenteert:
>
>Zomaar Gaat Zwaar
>
>Openlucht Hardcore-Metal-Thrash festival
>(Speelplein Begijnenborre)
>
>http://www.zomaargaatzwaar.be
>
>Bestel je tickets hier: http://www.zomaargaatzwaar.be/tickets.html
>
>
>CRIMSON FALLS (BE)
>(https://www.facebook.com/crimson.falls.9)
>
>SANITY'S RAGE ( BE )
>(https://www.facebook.com/SanitysRage)
>
>18 MILES ( NL )
>(https://www.facebook.com/18MILES)
>
>EMBRACE THE TIDE ( UK )
>(https://www.facebook.com/embracethetide)
>
>ALMOST FAILED (AT)
>(https://www.facebook.com/almostfailed)
>
>CHEAP DRUGS (BE)
>(https://www.facebook.com/CheapDrugs)
>
>RUNNING OUT ( NL )
>(https://www.facebook.com/RunningOutHardcore)
>
>END OF THE LINE (BE)
>https://www.facebook.com/pages/End-Of-The-Line/153275741828?fref=ts)
>
>DESERTER (BE)
>(https://www.facebook.com/DeserteRThrashmetal)
>
>CARRION (BE)
>(https://www.facebook.com/Carrionbe)
>
>THE WRONG KID DIED (BE)
>(https://www.facebook.com/pages/The-Wrong-Kid-Died/313436535348874?fref=ts)
>
>
>Damage : VVK : 10 euro / ADK : 14 euro
>
>
>Time table : 
>
>Doors : 12.00u
>
>The Wrong Kid Died 13.00-13.25u
>Carrion 14.00 - 14.25u
>Deserter 15.00 - 15.30u
>End of the Line 16.00 - 16.30u
>Running Out 17.00 - 17.30u
>Cheap Drugs 18.00 - 18.30u
>Almost Failed 19.00 - 19.35u
>Go Deep 20.05 - 20.20u
>This Routine Is Hell: 20.20-20.40
>18 miles 21.10 - 21.45u
>Sanity's Rage 22.15 - 22.55u
>Crimson Falls 23.25 - 00.10u

{{< album 20130907-zomaargaatzwaar >}}

bron: [Facebook](https://www.facebook.com/events/524633797584642/)
