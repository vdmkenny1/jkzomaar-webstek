+++
title = "Zomaar Zondigdt"
date = "2016-11-10"
+++
![banner](/img/20161110-zomaarzondigdt-banner.jpg)

Fuif ter ere van 42 jaar Zomaar.

>De 40J / ZomaaRatty organisatie gaat voor de laatste keer het beste van zichzelf geven om een onvergetelijk Zomaar feestje te bouwen. 
>Deze keer met 2 straffe optredens, een vette fuif, een chill lounge, een stylish cocktailbar, een gewone foodtruck en veel meer. 
>Van de kelder, via de tent, over het terras naar boven tot door het dak, overal feest.

bron: [Facebook](https://www.facebook.com/events/899864116791798/)

