+++
title = "Zomaar Gedrukt Maart 2003"
date = "2003-03-01"
+++

*Opgelet: De uitgavedatum van dit krantje wordt betwist, aangezien deze uitzonderlijk niet vermeld wordt op de voorpagina. 
De Archivarissen der Zomaer plaatsen dit boekje in begin 2003, heel waarschijnlijk uitgegeven in Maart, maar uw mening is welkom!*

{{< pdf "20030301-zomaargedrukt.pdf" >}}
