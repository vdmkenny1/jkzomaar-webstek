+++
title = "Indie Zomaar"
date = "2018-10-13"
+++

![banner](/img/20181013-indiezomaar-banner.jpg)

>Deze keer zijn wat lokale indie rock bands aan de beurt.
>
>First Floor - Britpop, Indiepop
>https://www.facebook.com/FirstFloorOfficial/
>
>JAKOMO - Indie Rock
>https://www.facebook.com/jakomoband/
>
>Afterparty met TWALLIE
>https://www.facebook.com/deejaytwallie/
>
>Doors: 20:00
>First band: 20:30
>Inkom: €2 voor JK Zomaar Leden, 3 euro voor niet-leden.
>Lid worden kan ter plekke voor slechts 5 euro!

{{< album 20181013-indiezomaar >}}

bron: [Facebook](https://www.facebook.com/events/1889866404413851/)
