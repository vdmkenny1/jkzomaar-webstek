+++
title = "Zomaar Goes Postal"
date = "2017-05-27"
+++

>Zomaar goes... bijt de spits af met een streepje post-metal!
>
>Line up:
>CATAYA | DE/BE | Instrumental Ambient Post-Metal
>https://www.facebook.com/cataya.band/
>
>PRESSURE COMMANDER | Antwerpen | Post Metal/Sludge
>https://www.facebook.com/pressurecommander/
>
>Doors: 20u30
>First Band: 21u
>
>Damage: Leden 4 euro, Niet-leden 5 euro.

![banner](/img/20170527-zomaargoespostal-banner.jpg)

{{< album 20170527-zomaargoespostal >}}

bron: [Facebook](https://www.facebook.com/events/1163042830460664/)
