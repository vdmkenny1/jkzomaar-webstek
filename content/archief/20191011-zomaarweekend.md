+++
title = "ZomaarWeekend 2019"
date = "2019-10-11"
+++
Bestuur + Crew gingen op weekend naar het heuvelachtige Tollembeek voor teambuilding, bezinning, en vergadering. Zeker niet om overmatig te drinken. Zeker niet.
{{< album 20191011-weekend >}}
