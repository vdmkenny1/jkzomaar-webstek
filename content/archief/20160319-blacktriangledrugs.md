+++
title = "Optreden Black Triangle Drugs"
date = "2016-03-19"
+++

![banner](/img/20160319-blacktriangledrugs-banner.jpg)

>Zaterdag 19 maart: Black Triangle Drugs live-gig in JK Zomaar! 
>Alternative mix van punk-rock, hardcore, stoner, psycho-folk, ska,... Come and check 'em out!
>
>Ingang = slechts 2€!
>
>Doors: 19:30
>Start show: 20:30
>Afterparty with 80's Punk!

{{< album 20160319-blacktriangledrugs >}}

bron: [Facebook](https://www.facebook.com/events/917428278372826/)
