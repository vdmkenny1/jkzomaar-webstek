+++
title = "ZomaaRock: Y.O.Y., 18 Miles Down"
date = "2018-05-05"
+++

![banner](/img/20180505-zomaarock-banner.jpg)

>Zaterdag 5 mei is het weer boenk erop!
>
>Y.O.Y (Why, Oh, Why):
>All Girls Punk Rock band uit het Aalsterse. Debuut optreden!
>(facebook link volgt)
>
>18 Miles Down:
>Stevige Rock met Americana invloeden!
>https://www.facebook.com/18MilesDown/
>
>Gratis inkom voor leden van JK Zomaar!
>2 Euro voor niet-leden. 
>(Lid worden kan ter plaatse voor slechts 5 euro, profijtelijk!)
>
>Doors om 20u, de eerste band rond 20u30.

{{< album 20180505-zomaarock >}}

bron: [Facebook](https://www.facebook.com/events/343247116165188/)
