+++
title = "Zomaar Gaat Zwaar 2014"
date = "2014-11-22"
+++
![banner](/img/20141122-zomaargaatzwaar-banner.jpg)

>22 november 2014 is een dag om naar uit te kijken!
>Want dan organiseert JK Zomaar de tweede editie Zomaar Gaat Zwaar.
>Deze editie mag je je verwachten aan harde basslijnen, strakke riffs en schorre kelen - ook wel gekend als Punk.
>
>
>Iedereen is welkom op 't Castelhof, te Sint-Martens-Bodegem.
>Op deze ietwat aparte locatie kan iedereen zich volledig laten gaan op volgende bands:
>
>- Lipstick Homicide
>- Cornflamesofficial
>- Your Highness
>- The Priceduifkes
>- We're Wolves
>- Coma Commander
>- Mountains To Move
>
>Voor meer info en updates check zeker onze website
>www.Zomaargaatzwaar.be

![logo](/img/20141122-zomaargaatzwaar-logo.jpg)

bron: [Facebook](https://www.facebook.com/events/1480293928894884/)
